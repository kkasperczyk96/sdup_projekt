
`timescale 1 ns / 1 ps

	module huffman_codec_ip_v1_0_S00_AXI #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXI data bus
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		// Width of S_AXI address bus
		parameter integer C_S_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line

		// Global Clock Signal
		input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		// Write channel Protection type. This signal indicates the
    		// privilege and security level of the transaction, and whether
    		// the transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_AWPROT,
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID,
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY,
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID,
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY,
		// Write response. This signal indicates the status
    		// of the write transaction.
		output wire [1 : 0] S_AXI_BRESP,
		// Write response valid. This signal indicates that the channel
    		// is signaling a valid write response.
		output wire  S_AXI_BVALID,
		// Response ready. This signal indicates that the master
    		// can accept a write response.
		input wire  S_AXI_BREADY,
		// Read address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		// Protection type. This signal indicates the privilege
    		// and security level of the transaction, and whether the
    		// transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_ARPROT,
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID,
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY,
		// Read data (issued by slave)
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		// Read response. This signal indicates the status of the
    		// read transfer.
		output wire [1 : 0] S_AXI_RRESP,
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
		output wire  S_AXI_RVALID,
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY
	);

	// AXI4LITE signals
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_awaddr;
	reg  	axi_awready;
	reg  	axi_wready;
	reg [1 : 0] 	axi_bresp;
	reg  	axi_bvalid;
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
	reg  	axi_arready;
	reg [C_S_AXI_DATA_WIDTH-1 : 0] 	axi_rdata;
	reg [1 : 0] 	axi_rresp;
	reg  	axi_rvalid;

	// Example-specific design signals
	// local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	// ADDR_LSB is used for addressing 32/64 bit registers/memories
	// ADDR_LSB = 2 for 32 bits (n downto 2)
	// ADDR_LSB = 3 for 64 bits (n downto 3)
	localparam integer ADDR_LSB = (C_S_AXI_DATA_WIDTH/32) + 1;
	localparam integer OPT_MEM_ADDR_BITS = 1;
	//----------------------------------------------
	//-- Signals for user logic register space example
	//------------------------------------------------
	//-- Number of Slave Registers 4
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg0;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg1;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg2;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg3;
	wire	 slv_reg_rden;
	wire	 slv_reg_wren;
	reg [C_S_AXI_DATA_WIDTH-1:0]	 reg_data_out;
	integer	 byte_index;

	// I/O Connections assignments

	assign S_AXI_AWREADY	= axi_awready;
	assign S_AXI_WREADY	= axi_wready;
	assign S_AXI_BRESP	= axi_bresp;
	assign S_AXI_BVALID	= axi_bvalid;
	assign S_AXI_ARREADY	= axi_arready;
	assign S_AXI_RDATA	= axi_rdata;
	assign S_AXI_RRESP	= axi_rresp;
	assign S_AXI_RVALID	= axi_rvalid;
	// Implement axi_awready generation
	// axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	// de-asserted when reset is low.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // slave is ready to accept write address when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_awready <= 1'b1;
	        end
	      else           
	        begin
	          axi_awready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_awaddr latching
	// This process is used to latch the address when both 
	// S_AXI_AWVALID and S_AXI_WVALID are valid. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awaddr <= 0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // Write Address latching 
	          axi_awaddr <= S_AXI_AWADDR;
	        end
	    end 
	end       

	// Implement axi_wready generation
	// axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	// de-asserted when reset is low. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_wready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_wready && S_AXI_WVALID && S_AXI_AWVALID)
	        begin
	          // slave is ready to accept write data when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_wready <= 1'b1;
	        end
	      else
	        begin
	          axi_wready <= 1'b0;
	        end
	    end 
	end       

	// Implement memory mapped register select and write logic generation
	// The write data is accepted and written to memory mapped registers when
	// axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	// select byte enables of slave registers while writing.
	// These registers are cleared when reset (active low) is applied.
	// Slave register write enable is asserted when valid address and data are available
	// and the slave is ready to accept the write address and write data.
	assign slv_reg_wren = axi_wready && S_AXI_WVALID && axi_awready && S_AXI_AWVALID;

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      slv_reg0 <= 0;
	      slv_reg1 <= 0;
	      //slv_reg2 <= 0;
	      slv_reg3 <= 0;
	    end 
	  else begin
	    if (slv_reg_wren)
	      begin
	        case ( axi_awaddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	          2'h0:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 0
	                slv_reg0[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h1:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 1
	                slv_reg1[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h2:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 2
	               // slv_reg2[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h3:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 3
	                slv_reg3[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          default : begin
	                      slv_reg0 <= slv_reg0;
	                      slv_reg1 <= slv_reg1;
	                      //slv_reg2 <= slv_reg2;
	                      slv_reg3 <= slv_reg3;
	                    end
	        endcase
	      end
	  end
	end    

	// Implement write response logic generation
	// The write response and response valid signals are asserted by the slave 
	// when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	// This marks the acceptance of address and indicates the status of 
	// write transaction.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_bvalid  <= 0;
	      axi_bresp   <= 2'b0;
	    end 
	  else
	    begin    
	      if (axi_awready && S_AXI_AWVALID && ~axi_bvalid && axi_wready && S_AXI_WVALID)
	        begin
	          // indicates a valid write response is available
	          axi_bvalid <= 1'b1;
	          axi_bresp  <= 2'b0; // 'OKAY' response 
	        end                   // work error responses in future
	      else
	        begin
	          if (S_AXI_BREADY && axi_bvalid) 
	            //check if bready is asserted while bvalid is high) 
	            //(there is a possibility that bready is always asserted high)   
	            begin
	              axi_bvalid <= 1'b0; 
	            end  
	        end
	    end
	end   

	// Implement axi_arready generation
	// axi_arready is asserted for one S_AXI_ACLK clock cycle when
	// S_AXI_ARVALID is asserted. axi_awready is 
	// de-asserted when reset (active low) is asserted. 
	// The read address is also latched when S_AXI_ARVALID is 
	// asserted. axi_araddr is reset to zero on reset assertion.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_arready <= 1'b0;
	      axi_araddr  <= 32'b0;
	    end 
	  else
	    begin    
	      if (~axi_arready && S_AXI_ARVALID)
	        begin
	          // indicates that the slave has acceped the valid read address
	          axi_arready <= 1'b1;
	          // Read address latching
	          axi_araddr  <= S_AXI_ARADDR;
	        end
	      else
	        begin
	          axi_arready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_arvalid generation
	// axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	// S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	// data are available on the axi_rdata bus at this instance. The 
	// assertion of axi_rvalid marks the validity of read data on the 
	// bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	// is deasserted on reset (active low). axi_rresp and axi_rdata are 
	// cleared to zero on reset (active low).  
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rvalid <= 0;
	      axi_rresp  <= 0;
	    end 
	  else
	    begin    
	      if (axi_arready && S_AXI_ARVALID && ~axi_rvalid)
	        begin
	          // Valid read data is available at the read data bus
	          axi_rvalid <= 1'b1;
	          axi_rresp  <= 2'b0; // 'OKAY' response
	        end   
	      else if (axi_rvalid && S_AXI_RREADY)
	        begin
	          // Read data is accepted by the master
	          axi_rvalid <= 1'b0;
	        end                
	    end
	end    

	// Implement memory mapped register select and read logic generation
	// Slave register read enable is asserted when valid address is available
	// and the slave is ready to accept the read address.
	assign slv_reg_rden = axi_arready & S_AXI_ARVALID & ~axi_rvalid;
	always @(*)
	begin
	      // Address decoding for reading registers
	      case ( axi_araddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	        2'h0   : reg_data_out <= slv_reg0;
	        2'h1   : reg_data_out <= slv_reg1;
	        2'h2   : reg_data_out <= slv_reg2;
	        2'h3   : reg_data_out <= slv_reg3;
	        default : reg_data_out <= 0;
	      endcase
	end

	// Output register or memory read data
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rdata  <= 0;
	    end 
	  else
	    begin    
	      // When there is a valid read address (S_AXI_ARVALID) with 
	      // acceptance of read address by the slave (axi_arready), 
	      // output the read dada 
	      if (slv_reg_rden)
	        begin
	          axi_rdata <= reg_data_out;     // register read data
	        end   
	    end
	end    

	// Add user logic here
	//Reset signal for cordic processor
    wire ARESET;
    assign ARESET = ~S_AXI_ARESETN;

    wire [C_S_AXI_DATA_WIDTH-1:0] slv_wire2;
    always@(posedge S_AXI_ACLK)
    begin
        slv_reg2 <= slv_wire2;
    end
    
    assign slv_wire2[31:11] = 21'b0;

    huffman_codec huffman_codec_inst(
        S_AXI_ACLK,     //clock
        slv_reg0[0],    //data_enable
		ARESET,			// reset
        slv_reg0[8:1],   //data_in
        slv_reg0[16:9],  //data_count
        slv_wire2[0],    //coded_out
        slv_wire2[8:1],  //decoded_out
        slv_wire2[9],    //coded_out_data_ready
        slv_wire2[10],   //decoded_out_data_ready
		slv_reg0[17],	 //ready_for_coded_data
		slv_reg0[18]	 //ready_for_decoded_data
    );

	// User logic ends

	endmodule


// huffman coder module

`define CODER_INIT              3'b000
`define CODER_DATA_LOADING     3'b001
`define CODING_STATE_1         3'b010
`define CODING_STATE_2		   3'b011
`define CODING_STATE_3 			3'b100
`define CODER_SENDING_DATA            3'b101

module huffman_coder(
    input wire clock,
    input wire reset,
    input wire coder_data_enable,
    input wire [7:0] input_data_stream,
    input wire [7:0] input_data_stream_length,
    input wire [31:0] code_list_stream,
    input wire [7:0] codes_length_stream,
    input wire [7:0] symbols_stream,
    input wire [7:0] symbols_amount,
    output reg coder_data_ready,
    output reg coded_bit_stream
    );
    
    integer input_data_length = 32'h0;
    integer symbols_amount_xd = 32'h0;
    reg [2:0] state;
    //reg [1:0] coding_state;

    reg [31:0] code_list [0:99];
    reg [7:0] codes_lengths [0:99];
    reg [7:0] symbols [0:99];
    reg [7:0] input_data [0:999];
    reg code_bit_tab [0:999];
  
    integer init_data_counter = 32'h0;
    integer character_stream_counter = 32'h0;
    integer coder_iterator = 32'h0;
    integer i = 32'h0;
    integer l = 32'h0;
    integer j = 32'h0;
    integer k = 32'h0;
    
    integer position = 32'h0;
  
    always @(posedge clock or negedge reset) begin
        if(!reset) begin
            state = `CODER_INIT;
            //coding_state = `CODING_STATE_1;
        end
		else begin
        case(state)
            `CODER_INIT: begin
                if(coder_data_enable)begin
                    input_data_length = input_data_stream_length;
                    symbols_amount_xd = symbols_amount;
                    code_list[init_data_counter] = code_list_stream;
                    codes_lengths[init_data_counter] = codes_length_stream;
                    symbols[init_data_counter] = symbols_stream;
                    init_data_counter = init_data_counter + 1;
                    if(init_data_counter == symbols_amount) begin
                        state = `CODER_DATA_LOADING;
                        coder_iterator = 0;
                        k = 0;
                       // $display("Finished loading init data");         
                    end
                end
            end
            `CODER_DATA_LOADING: begin
                if(coder_data_enable)begin
                    input_data[character_stream_counter] = input_data_stream;
                    character_stream_counter = character_stream_counter + 1;
                    state = `CODER_DATA_LOADING;
                end
                if(character_stream_counter == input_data_length) begin
                    state = `CODING_STATE_1;
                    //$display("Finished loading input data");
//                    for(i=0;i<input_data_stream_length;i=i+1)$display("input characters: ", input_data[i]); 
                end
            end
                    `CODING_STATE_1: begin
                        //$display("STATE 1");
                        j = 0;
                        if(coder_iterator < input_data_length) begin
                            position = 0;
                            state = `CODING_STATE_2;
                        end
                        else begin
                            state = `CODER_SENDING_DATA;
                            j = 0;
                        end
                    end
                    `CODING_STATE_2: begin
                        //$display("STATE 2");
                        if((symbols[position] != input_data[coder_iterator])) begin
                            position = position + 1;
                            state = `CODING_STATE_2; 
                        end
                        else begin
                           state = `CODING_STATE_3; 
                           l = 1;
                        end
                    end
                    `CODING_STATE_3: begin
                        //$display("STATE 3");
                        if(j < codes_lengths[position]) begin
                            if((code_list[position] & l) != 0) begin
                                //$display("1");
                                code_bit_tab[k] = 1;
                                k = k + 1;
                            end
                            else begin
                               // $display("0");
                                code_bit_tab[k] = 0;
                                k = k + 1;
                            end
                            // multiply by 2
                            l = l << 1;
                            j = j + 1;
                            state = `CODING_STATE_3; 
                        end
                        else begin
                            coder_iterator = coder_iterator + 1;
                            state = `CODING_STATE_1;
                        end
                    end         
            `CODER_SENDING_DATA: begin
                if(j < k) begin
                    //$display("SENDING");
                    coder_data_ready = 1;
                    coded_bit_stream = code_bit_tab[j];
                    j = j + 1;
                    state = `CODER_SENDING_DATA;
                end
                else begin
                    coder_data_ready = 0;
                    state = `CODER_INIT;
                end
            end
            default: begin
            end
        endcase
		end


    end
endmodule



// huffman decoder module

`define DECODER_INIT            3'b000
`define DECODER_DATA_LOADING    3'b001
`define DECODING_STATE_1 3'b010
`define DECODING_STATE_2 3'b011
`define DECODING_STATE_3 3'b100
`define DECODER_DATA_SENDING    3'b101



module huffman_decoder(
    input wire clock,
    input wire reset,
    input wire decoder_data_enable,
    input wire input_data_stream,
    input wire [31:0] input_data_stream_length,
    input wire [31:0] code_list_stream,
    input wire [7:0] codes_length_stream,
    input wire [7:0] symbols_stream,
    input wire [7:0] symbols_amount,
    output reg decoder_data_ready,
    output reg [7:0] decoded_number
    );
    
    reg [2:0] state;
    //reg [1:0] decoding_state;
    
    reg [31:0] code_list [0:99];
    reg [7:0] codes_lengths [0:99];
    reg [7:0] symbols [0:99];
    reg input_data [0:999];
    reg [31:0] input_data_length;
    reg [7:0] symbol_amount_var;
    reg [7:0] decoded_characters_array [0:99];
    
    integer bit_stream_loader = 32'h0;
    integer init_data_counter = 32'h0;
    integer decoded_characters_counter = 32'h0;
    integer i = 32'h0;
    
    integer bit_position = 32'h0;
    integer code_position = 32'h0;
    
    integer code_index = 8'h0;
    integer bit_index = 8'h0;
    integer code_value = 64'h0;
    integer character_length = 8'h0;
    
    integer sequence_to_and = 8'h0;
    
    always @(posedge clock or negedge reset) begin
        if(!reset) begin
            state = `DECODER_INIT;
            //decoding_state = `DECODING_STATE_1;
        end
		else begin
		    case(state)
            `DECODER_INIT: begin
                if(decoder_data_enable) begin
                    input_data_length = input_data_stream_length;
                    symbol_amount_var = symbols_amount;
                    code_list[init_data_counter] = code_list_stream;
                    codes_lengths[init_data_counter] = codes_length_stream;
                    symbols[init_data_counter] = symbols_stream;
                    init_data_counter = init_data_counter + 1;
                    if(init_data_counter == symbol_amount_var) begin
                        character_length = 1;
                        decoded_characters_counter = 0;
                        i = 0;
                        bit_position = 0;
                        state = `DECODER_DATA_LOADING;
                     //   $display("Finished loading init data");
    //                    for(i=0;i<27;i=i+1) begin
    //                        $display("code_list: ", code_list[i]);
    //                        $display("codes_lengths: ", codes_lengths[i]);
    //                        $display("symbols: ", symbols[i]);
    //                    end
                    end
                    else begin
                        state = `DECODER_INIT;
                    end
                end
            end
            `DECODER_DATA_LOADING: begin
                if(decoder_data_enable) begin
                    input_data[bit_stream_loader] = input_data_stream;
                    bit_stream_loader = bit_stream_loader + 1;
                    if(bit_stream_loader == input_data_length) begin
                        state = `DECODING_STATE_1;
      //                  $display("Finished loading input data");
    //                    for(i=0;i<169;i=i+1) begin
    //                        $display("bit: ", input_data[i]);
    //                    end
                    end
                    else begin
                        state = `DECODER_DATA_LOADING;
                    end
                end
                else begin
                    state = `DECODER_DATA_LOADING;
                end
            end    
                    `DECODING_STATE_1: begin
                        //$display("State 1");
                        if(bit_position < input_data_length) begin
                            state = `DECODING_STATE_2;
                            code_index = character_length;
                            bit_index = bit_position;
                            code_value = 0;
                            code_position = 0;                          
                        end
                        else begin
                            i = 0;
                            state = `DECODER_DATA_SENDING; 
                        end
                    end
                    `DECODING_STATE_2: begin
                        //$display("State 2");
                        if(code_index != 0) begin
                            //code_value = code_value + ((2**(code_index-1))*input_data[bit_index]);
                            if(code_index-1 == 0) begin
                                code_value = code_value + input_data[bit_index];
                            end
                            else begin
                                code_value = code_value + ((2<<(code_index-2))*input_data[bit_index]);
                            end
                            code_index = code_index - 1;
                            bit_index = bit_index - 1;
                            state = `DECODING_STATE_2;
    //                        $display("code_value: ", code_value);
                        end
                        else begin
                            state = `DECODING_STATE_3;
                        end
                    end
                    `DECODING_STATE_3: begin
                        //$display("State 3");
                        //$display("Bit: ", input_data[bit_position]);
                        if(code_position < symbol_amount_var) begin
                            if(character_length == 0) begin
                                sequence_to_and = 0;
                            end
                            else begin
                                sequence_to_and = (2 << (character_length-1)) - 1;
                            end
                            if(((code_list[code_position])&sequence_to_and) == code_value) begin
                                if(codes_lengths[code_position] > character_length) begin
                                    //$display("I want more bits");
                                    character_length = character_length + 1;
                                end
                                else begin
                                    character_length = 1;                   
                                    decoded_characters_array[decoded_characters_counter] = symbols[code_position];
                                    decoded_characters_counter = decoded_characters_counter + 1;
                                    //$display("Decoded symbol: ", symbols[code_position]);
                                end
                                //break;
                                code_position = symbol_amount_var;
                            end 
                            code_position = code_position + 1;
                            state = `DECODING_STATE_3;
                        end
                        else begin
                            state = `DECODING_STATE_1;
                            bit_position = bit_position + 1;
                        end
                    end
            `DECODER_DATA_SENDING: begin
                if(i < decoded_characters_counter) begin
                    decoder_data_ready = 1;
                    decoded_number = decoded_characters_array[i];
                    i = i + 1;
                    state = `DECODER_DATA_SENDING;
                end
                else begin
                    decoder_data_ready = 0;
                    state = `DECODER_INIT;
                end
            end
            default: begin
            end
         endcase
		end


    end
endmodule

// huffman codec module

/// defines using in huffman tree building

`define CODEC_LOAD_DATA         4'b0000 
`define TREE_LOAD_DATA          4'b0001 
`define TREE_CALC               4'b0010
`define CODER_LOAD_TREE_DATA    4'b0011
`define CODER_LOAD_INPUT_DATA   4'b0100
`define CODER_CALC              4'b0101
`define DECODER_LOAD_TREE_DATA  4'b0110
`define DECODER_LOAD_INPUT_DATA 4'b0111
`define DECODER_CALC            4'b1000
`define WRITE_DATA_OUT          4'b1001

module huffman_codec(
    input wire clock,
    input wire data_enable,
    input wire reset,
    input wire [7:0] data_in,
    input wire [7:0] data_count,
    output reg coded_out,
    output reg [7:0] decoded_out,
    output reg coded_out_data_ready,
    output reg decoded_out_data_ready,
    input wire ready_for_coded_data,
    input wire ready_for_decoded_data
    );
    
/// define symbols tables for Polish alphabet
parameter symbols_amount = 50;
reg [7:0] symbols [0:symbols_amount - 1];

/// global variables
parameter max_data_count = 100;
reg [3:0] state;
reg [7:0] data_input_tab [0:max_data_count-1];
reg [7:0] input_data_count;
reg previous_data_enable;
reg ready_for_coded_data_prev;
reg ready_for_decoded_data_prev;

integer bit_counter = 32'h0;
integer decoded_symbols_number = 32'h0;

/// define  tables for tree
//reg [7:0] tree_symbol_in;
//reg [31:0] tree_probability_in;
//reg [7:0] tree_symbols_length;
//reg tree_data_enable;
//wire tree_data_ready;
//wire [31:0] tree_codes;
//wire [7:0] tree_codes_length;
//reg tree_data_ready_output;

/// define output tables for coder
reg [7:0] coder_codes_tab [0:symbols_amount-1]; 
reg [7:0] coder_codes_tab_length [0:symbols_amount-1];
reg [7:0] coder_data_input; 
//reg [7:0] coder_data_input_tab [0:max_data_count-1];
reg [31:0] coder_code_in;
reg [7:0] coder_code_length_in;
reg [7:0] coder_symbols_in;
//reg [7:0] coder_symbols_length_in;
reg coder_data_enable;
wire coder_data_ready;
wire coder_bit_output;
reg coder_data_ready_output;
reg [7:0] coder_symbols_amount;

// define tables for decoder
parameter max_decoder_input_bit_count = 1000;
reg decoder_bit_input [0:max_decoder_input_bit_count-1];
reg decoder_data_enable;
reg decoder_data_in;
reg [31:0] decoder_data_length_in;
reg [31:0] decoder_code_in;
reg [7:0] decoder_code_length_in;
reg [7:0] decoder_symbols_in;
reg [7:0] decoder_symbols_amount;
wire [7:0] decoder_decoded_number;
wire decoder_data_ready;
reg decoder_data_ready_output;

reg [7:0] decoded_output [0:max_data_count-1];

/// iterators
integer i = 32'h0;
integer k = 32'h0;

//huffman_tree tree(
//        .clock(clock),
//        .reset(reset),
//        .tree_data_enable(tree_data_enable),
//        .symbol_in(tree_symbol_in),
//        .probability_in(tree_probability_in),
//        .symbols_length(tree_symbols_length),
//        .tree_data_ready(tree_data_ready),
//        .code_out(tree_codes),
//        .code_out_length(tree_codes_length)
//        );   
        
huffman_coder coder(
        .clock(clock),
        .reset(reset),
        .coder_data_enable(coder_data_enable),
        .input_data_stream(coder_data_input),
        .input_data_stream_length(input_data_count),
        .code_list_stream(coder_code_in),
        .codes_length_stream(coder_code_length_in),
        .symbols_stream(coder_symbols_in),
        .symbols_amount(coder_symbols_amount),
        .coder_data_ready(coder_data_ready),
        .coded_bit_stream(coder_bit_output) 
        );
            
huffman_decoder decoder(
        .clock(clock),
        .reset(reset),
        .decoder_data_enable(decoder_data_enable),
        .input_data_stream(decoder_data_in),
        .input_data_stream_length(decoder_data_length_in),
        .code_list_stream(decoder_code_in),
        .codes_length_stream(decoder_code_length_in),
        .symbols_stream(decoder_symbols_in),
        .symbols_amount(decoder_symbols_amount),
        .decoder_data_ready(decoder_data_ready),
        .decoded_number(decoder_decoded_number)
        );
        
        initial begin
            symbols[0] = 32;
            symbols[1] = 44;
            symbols[2] = 46;
            symbols[3] = 59;
            symbols[4] = 65;
            symbols[5] = 66;
            symbols[6] = 67;
            symbols[7] = 68;
            symbols[8] = 69;
            symbols[9] = 70;
            symbols[10] = 71;
            symbols[11] = 72;
            symbols[12] = 73;
            symbols[13] = 74;
            symbols[14] = 75;
            symbols[15] = 76;
            symbols[16] = 77;
            symbols[17] = 78;
            symbols[18] = 79;
            symbols[19] = 80;
            symbols[20] = 82;
            symbols[21] = 83;
            symbols[22] = 84;
            symbols[23] = 85;
            symbols[24] = 87;
            symbols[25] = 89;
            symbols[26] = 90;
            symbols[27] = 97;
            symbols[28] = 98;
            symbols[29] = 99;
            symbols[30] = 100;
            symbols[31] = 101;
            symbols[32] = 102;
            symbols[33] = 103;
            symbols[34] = 104;
            symbols[35] = 105;
            symbols[36] = 106;
            symbols[37] = 107;
            symbols[38] = 108;
            symbols[39] = 109;
            symbols[40] = 110;
            symbols[41] = 111;
            symbols[42] = 112;
            symbols[43] = 114;
            symbols[44] = 115;
            symbols[45] = 116;
            symbols[46] = 117;
            symbols[47] = 119;
            symbols[48] = 121;
            symbols[49] = 122;
            coder_codes_tab[0] = 7;
            coder_codes_tab[1] = 122;
            coder_codes_tab[2] = 83;
            coder_codes_tab[3] = 26;
            coder_codes_tab[4] = 27;
            coder_codes_tab[5] = 58;
            coder_codes_tab[6] = 42;
            coder_codes_tab[7] = 30;
            coder_codes_tab[8] = 20;
            coder_codes_tab[9] = 4;
            coder_codes_tab[10] = 10;
            coder_codes_tab[11] = 100;
            coder_codes_tab[12] = 5;
            coder_codes_tab[13] = 75;
            coder_codes_tab[14] = 50;
            coder_codes_tab[15] = 1;
            coder_codes_tab[16] = 60;
            coder_codes_tab[17] = 24;
            coder_codes_tab[18] = 13;
            coder_codes_tab[19] = 18;
            coder_codes_tab[20] = 29;
            coder_codes_tab[21] = 51;
            coder_codes_tab[22] = 28;
            coder_codes_tab[23] = 11;
            coder_codes_tab[24] = 61;
            coder_codes_tab[25] = 33;
            coder_codes_tab[26] = 14;
            coder_codes_tab[27] = 0;
            coder_codes_tab[28] = 90;
            coder_codes_tab[29] = 43;
            coder_codes_tab[30] = 38;
            coder_codes_tab[31] = 9;
            coder_codes_tab[32] = 68;
            coder_codes_tab[33] = 74;
            coder_codes_tab[34] = 36;
            coder_codes_tab[35] = 25;
            coder_codes_tab[36] = 8;
            coder_codes_tab[37] = 6;
            coder_codes_tab[38] = 62;
            coder_codes_tab[39] = 2;
            coder_codes_tab[40] = 12;
            coder_codes_tab[41] = 21;
            coder_codes_tab[42] = 40;
            coder_codes_tab[43] = 49;
            coder_codes_tab[44] = 35;
            coder_codes_tab[45] = 34;
            coder_codes_tab[46] = 19;
            coder_codes_tab[47] = 3;
            coder_codes_tab[48] = 17;
            coder_codes_tab[49] = 22;
            
            coder_codes_tab_length[0] = 3;
            coder_codes_tab_length[1] = 7;
            coder_codes_tab_length[2] = 7;
            coder_codes_tab_length[3] = 7;
            coder_codes_tab_length[4] = 5;
            coder_codes_tab_length[5] = 7;
            coder_codes_tab_length[6] = 6;
            coder_codes_tab_length[7] = 6;
            coder_codes_tab_length[8] = 5;
            coder_codes_tab_length[9] = 7;
            coder_codes_tab_length[10] = 7;
            coder_codes_tab_length[11] = 7;
            coder_codes_tab_length[12] = 5;
            coder_codes_tab_length[13] = 7;
            coder_codes_tab_length[14] = 6;
            coder_codes_tab_length[15] = 6;
            coder_codes_tab_length[16] = 6;
            coder_codes_tab_length[17] = 5;
            coder_codes_tab_length[18] = 5;
            coder_codes_tab_length[19] = 6;
            coder_codes_tab_length[20] = 6;
            coder_codes_tab_length[21] = 6;
            coder_codes_tab_length[22] = 6;
            coder_codes_tab_length[23] = 7;
            coder_codes_tab_length[24] = 6;
            coder_codes_tab_length[25] = 6;
            coder_codes_tab_length[26] = 5;
            coder_codes_tab_length[27] = 4;
            coder_codes_tab_length[28] = 7;
            coder_codes_tab_length[29] = 6;
            coder_codes_tab_length[30] = 6;
            coder_codes_tab_length[31] = 5;
            coder_codes_tab_length[32] = 7;
            coder_codes_tab_length[33] = 7;
            coder_codes_tab_length[34] = 7;
            coder_codes_tab_length[35] = 5;
            coder_codes_tab_length[36] = 6;
            coder_codes_tab_length[37] = 6;
            coder_codes_tab_length[38] = 6;
            coder_codes_tab_length[39] = 6;
            coder_codes_tab_length[40] = 5;
            coder_codes_tab_length[41] = 5;
            coder_codes_tab_length[42] = 6;
            coder_codes_tab_length[43] = 6;
            coder_codes_tab_length[44] = 6;
            coder_codes_tab_length[45] = 6;
            coder_codes_tab_length[46] = 7;
            coder_codes_tab_length[47] = 6;
            coder_codes_tab_length[48] = 6;
            coder_codes_tab_length[49] = 5;

            
        end

        always @(posedge clock or negedge reset) begin
            if(!reset) begin
                state = `CODEC_LOAD_DATA;
                input_data_count = 8'b0;
                previous_data_enable = 1'b0;
                //tree_data_enable = 1'b0;
                coder_data_enable = 1'b0;
                decoder_data_enable = 1'b0;
                ready_for_coded_data_prev = 1'b0;
                ready_for_decoded_data_prev = 1'b0;
                coded_out_data_ready = 1'b0;
                decoded_out_data_ready = 1'b0;
                decoded_out = 8'b0;
                coded_out = 1'b0;
            end
			else begin
            case(state)
                `CODEC_LOAD_DATA: begin
                    if(data_enable != previous_data_enable) begin
                        previous_data_enable = data_enable;
                        input_data_count = data_count;
                        coder_symbols_amount = symbols_amount;
                        decoder_symbols_amount = symbols_amount;
                        if(i < data_count) begin
                            data_input_tab[i] = data_in;
                            i = i + 1;
                            state = `CODEC_LOAD_DATA;
                        end
                        else begin
                            i = 0;
                            //tree_symbols_length = 50;
                            //state = `TREE_LOAD_DATA;
                            state = `CODER_LOAD_TREE_DATA;
                        end
                    end
                    else begin
                        state = `CODEC_LOAD_DATA;
                    end
                end
                
//                `TREE_LOAD_DATA: begin
//                    if(i < symbols_amount) begin
//                        tree_data_enable = 1;
//                        tree_symbols_length = 50;
//                        tree_symbol_in = symbols[i];
//                        tree_probability_in = probabilities[i];
//                        i = i + 1;
//                        state = `TREE_LOAD_DATA;
//                    end
//                    else begin
//                        i = 0;
//                        state = `TREE_CALC;
//                    end
//                end
                
//                `TREE_CALC: begin
//                    tree_data_ready_output = tree_data_ready;
//                    if(tree_data_ready_output) begin
//                        if(i < symbols_amount) begin
//                            coder_codes_tab[i] = tree_codes;
//                            coder_codes_tab_length[i] = tree_codes_length;
//                            i = i + 1;
//                            state = `TREE_CALC;
//                        end
//                        else begin
//                        //for(k=0; k < symbols_amount; k = k + 1) $display("symbol: ", symbols[k], " code: ", coder_codes_tab[k], " length: ", coder_codes_tab_length[k]);
//                            state = `CODER_LOAD_TREE_DATA;
//                            i = 0;
//                        end
//                    end
//                end
                
                `CODER_LOAD_TREE_DATA: begin
                    if(i < symbols_amount) begin
                        coder_data_enable = 1;
                        coder_code_in = coder_codes_tab[i];
                        coder_code_length_in = coder_codes_tab_length[i];
                        coder_symbols_in = symbols[i];
                        i = i + 1;
                        state = `CODER_LOAD_TREE_DATA;
                    end
                    else begin
                        i = 0;
                        coder_data_enable = 0;
                        state = `CODER_LOAD_INPUT_DATA;
                    end
                end
                
                `CODER_LOAD_INPUT_DATA: begin
                    if(i < input_data_count) begin
                        coder_data_enable = 1;
                        coder_data_input = data_input_tab[i];
                        i = i + 1;
                        state = `CODER_LOAD_INPUT_DATA;
                    end
                    else begin
                        i = 0;
                        state = `CODER_CALC;
                    end
                end

                `CODER_CALC: begin
                    coder_data_ready_output = coder_data_ready;
                    if(coder_data_ready_output) begin
                        decoder_bit_input[bit_counter] = coder_bit_output;
                        //$display("bit: ", decoder_bit_input[bit_counter]);
                        bit_counter = bit_counter + 1; 
                        state = `CODER_CALC;
                    end
                    else if(coder_data_ready_output == 0 && bit_counter > 0) begin
                        i = 0;
                        decoder_data_length_in = bit_counter;
                        state = `DECODER_LOAD_TREE_DATA;
                    end
                end  
                
                `DECODER_LOAD_TREE_DATA: begin
                     if(i < symbols_amount) begin
                         decoder_data_enable = 1;
                         decoder_code_in = coder_codes_tab[i];
                         decoder_code_length_in = coder_codes_tab_length[i];
                         decoder_symbols_in = symbols[i];
                         i = i + 1;
                         state = `DECODER_LOAD_TREE_DATA;
                     end
                     else begin
                         i = 0;
                         decoder_data_enable = 0;
                         state = `DECODER_LOAD_INPUT_DATA;
                     end  
                end
                
                `DECODER_LOAD_INPUT_DATA: begin
                    if(i < bit_counter) begin
                        decoder_data_enable = 1;
                        //decoder_data_length_in = bit_counter;
                        decoder_data_in = decoder_bit_input[i];
                        i = i + 1;
                        state = `DECODER_LOAD_INPUT_DATA;
                    end
                    else begin
                        i = 0;
                        state = `DECODER_CALC;
                    end
                end
                
                `DECODER_CALC: begin
                    decoder_data_ready_output = decoder_data_ready;
                    if(decoder_data_ready_output) begin
                        decoded_output[i] = decoder_decoded_number;
                        decoded_symbols_number = decoded_symbols_number+1;
                        //$display("Decoded: ",decoded_output[i],"counter: ",decoded_symbols_number); 
                        i = i + 1;
                        state = `DECODER_CALC;
                    end
                    else if(decoder_data_ready_output == 0 && decoded_symbols_number > 0) begin
                        i = 0;
                        k = 0;
                        state = `WRITE_DATA_OUT;
                    end
                end
                
                `WRITE_DATA_OUT: begin
                    if(i < bit_counter) begin
                        coded_out_data_ready = 1;
                        if(ready_for_coded_data != ready_for_coded_data_prev) begin
                            ready_for_coded_data_prev = ready_for_coded_data;
                            coded_out = decoder_bit_input[i];
                            i = i+1;
                        end
                        state = `WRITE_DATA_OUT;
                    end
                    else begin
                        coded_out_data_ready = 0;
                        state = `CODEC_LOAD_DATA;
                    end
                    if(k < decoded_symbols_number) begin
                        decoded_out_data_ready = 1;
                        if(ready_for_decoded_data != ready_for_decoded_data_prev) begin
                            ready_for_decoded_data_prev = ready_for_decoded_data;
                            decoded_out = decoded_output[k];
                            k = k+1;
                        end
                        state = `WRITE_DATA_OUT;
                    end
                    else begin
                        decoded_out_data_ready = 0;
                    end
                end
                
                default: begin
                    // empty
                end
            endcase
			end
            

        end    
endmodule


