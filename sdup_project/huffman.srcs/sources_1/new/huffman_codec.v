`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.05.2019 10:17:09
// Design Name: 
// Module Name: huffman_codec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

/// defines using in huffman tree building

`define CODEC_LOAD_DATA         4'b0000 
`define TREE_LOAD_DATA          4'b0001 
`define TREE_CALC               4'b0010
`define CODER_LOAD_TREE_DATA    4'b0011
`define CODER_LOAD_INPUT_DATA   4'b0100
`define CODER_CALC              4'b0101
`define DECODER_LOAD_TREE_DATA  4'b0110
`define DECODER_LOAD_INPUT_DATA 4'b0111
`define DECODER_CALC            4'b1000
`define WRITE_DATA_OUT          4'b1001

module huffman_codec(
    input wire clock,
    input wire data_enable,
    input wire reset,
    input wire [7:0] data_in,
    input wire [7:0] data_count,
    output reg coded_out,
    output reg [7:0] decoded_out,
    output reg coded_out_data_ready,
    output reg decoded_out_data_ready,
    input wire ready_for_coded_data,
    input wire ready_for_decoded_data
    );
    
/// define symbols tables for Polish alphabet
parameter symbols_amount = 27;//parameter symbols_amount = 50;
reg [7:0] symbols [0:symbols_amount - 1];

/// global variables
parameter max_data_count = 100;
reg [3:0] state;
reg [7:0] data_input_tab [0:max_data_count-1];
reg [7:0] input_data_count;
reg previous_data_enable;
reg ready_for_coded_data_prev;
reg ready_for_decoded_data_prev;

integer bit_counter = 32'h0;
integer decoded_symbols_number = 32'h0;

/// define  tables for tree
//reg [7:0] tree_symbol_in;
//reg [31:0] tree_probability_in;
//reg [7:0] tree_symbols_length;
//reg tree_data_enable;
//wire tree_data_ready;
//wire [31:0] tree_codes;
//wire [7:0] tree_codes_length;
//reg tree_data_ready_output;

/// define output tables for coder
reg [7:0] coder_codes_tab [0:symbols_amount-1]; 
reg [7:0] coder_codes_tab_length [0:symbols_amount-1];
reg [7:0] coder_data_input; 
//reg [7:0] coder_data_input_tab [0:max_data_count-1];
reg [31:0] coder_code_in;
reg [7:0] coder_code_length_in;
reg [7:0] coder_symbols_in;
//reg [7:0] coder_symbols_length_in;
reg coder_data_enable;
wire coder_data_ready;
wire coder_bit_output;
reg coder_data_ready_output;
reg [7:0] coder_symbols_amount;

// define tables for decoder
parameter max_decoder_input_bit_count = 1000;
reg decoder_bit_input [0:max_decoder_input_bit_count-1];
reg decoder_data_enable;
reg decoder_data_in;
reg [31:0] decoder_data_length_in;
reg [31:0] decoder_code_in;
reg [7:0] decoder_code_length_in;
reg [7:0] decoder_symbols_in;
reg [7:0] decoder_symbols_amount;
wire [7:0] decoder_decoded_number;
wire decoder_data_ready;
reg decoder_data_ready_output;

reg [7:0] decoded_output [0:max_data_count-1];

/// iterators
integer i = 32'h0;
integer k = 32'h0;

//huffman_tree tree(
//        .clock(clock),
//        .reset(reset),
//        .tree_data_enable(tree_data_enable),
//        .symbol_in(tree_symbol_in),
//        .probability_in(tree_probability_in),
//        .symbols_length(tree_symbols_length),
//        .tree_data_ready(tree_data_ready),
//        .code_out(tree_codes),
//        .code_out_length(tree_codes_length)
//        );   
        
huffman_coder coder(
        .clock(clock),
        .reset(reset),
        .coder_data_enable(coder_data_enable),
        .input_data_stream(coder_data_input),
        .input_data_stream_length(input_data_count),
        .code_list_stream(coder_code_in),
        .codes_length_stream(coder_code_length_in),
        .symbols_stream(coder_symbols_in),
        .symbols_amount(coder_symbols_amount),
        .coder_data_ready(coder_data_ready),
        .coded_bit_stream(coder_bit_output) 
        );
            
huffman_decoder decoder(
        .clock(clock),
        .reset(reset),
        .decoder_data_enable(decoder_data_enable),
        .input_data_stream(decoder_data_in),
        .input_data_stream_length(decoder_data_length_in),
        .code_list_stream(decoder_code_in),
        .codes_length_stream(decoder_code_length_in),
        .symbols_stream(decoder_symbols_in),
        .symbols_amount(decoder_symbols_amount),
        .decoder_data_ready(decoder_data_ready),
        .decoded_number(decoder_decoded_number)
        );
        
        initial begin
            symbols[0] = 32;
            symbols[1] = 44;
            symbols[2] = 46;
            symbols[3] = 59;
            symbols[4] = 65;
            symbols[5] = 66;
            symbols[6] = 67;
            symbols[7] = 68;
            symbols[8] = 69;
            symbols[9] = 70;
            symbols[10] = 71;
            symbols[11] = 72;
            symbols[12] = 73;
            symbols[13] = 74;
            symbols[14] = 75;
            symbols[15] = 76;
            symbols[16] = 77;
            symbols[17] = 78;
            symbols[18] = 79;
            symbols[19] = 80;
            symbols[20] = 82;
            symbols[21] = 83;
            symbols[22] = 84;
            symbols[23] = 85;
            symbols[24] = 87;
            symbols[25] = 89;
            symbols[26] = 90;
//            symbols[27] = 97;
//            symbols[28] = 98;
//            symbols[29] = 99;
//            symbols[30] = 100;
//            symbols[31] = 101;
//            symbols[32] = 102;
//            symbols[33] = 103;
//            symbols[34] = 104;
//            symbols[35] = 105;
//            symbols[36] = 106;
//            symbols[37] = 107;
//            symbols[38] = 108;
//            symbols[39] = 109;
//            symbols[40] = 110;
//            symbols[41] = 111;
//            symbols[42] = 112;
//            symbols[43] = 114;
//            symbols[44] = 115;
//            symbols[45] = 116;
//            symbols[46] = 117;
//            symbols[47] = 119;
//            symbols[48] = 121;
//            symbols[49] = 122;
            coder_codes_tab[0] = 7;
            coder_codes_tab[1] = 122;
            coder_codes_tab[2] = 83;
            coder_codes_tab[3] = 26;
            coder_codes_tab[4] = 27;
            coder_codes_tab[5] = 58;
            coder_codes_tab[6] = 42;
            coder_codes_tab[7] = 30;
            coder_codes_tab[8] = 20;
            coder_codes_tab[9] = 4;
            coder_codes_tab[10] = 10;
            coder_codes_tab[11] = 100;
            coder_codes_tab[12] = 5;
            coder_codes_tab[13] = 75;
            coder_codes_tab[14] = 50;
            coder_codes_tab[15] = 1;
            coder_codes_tab[16] = 60;
            coder_codes_tab[17] = 24;
            coder_codes_tab[18] = 13;
            coder_codes_tab[19] = 18;
            coder_codes_tab[20] = 29;
            coder_codes_tab[21] = 51;
            coder_codes_tab[22] = 28;
            coder_codes_tab[23] = 11;
            coder_codes_tab[24] = 61;
            coder_codes_tab[25] = 33;
            coder_codes_tab[26] = 14;
//            coder_codes_tab[27] = 0;
//            coder_codes_tab[28] = 90;
//            coder_codes_tab[29] = 43;
//            coder_codes_tab[30] = 38;
//            coder_codes_tab[31] = 9;
//            coder_codes_tab[32] = 68;
//            coder_codes_tab[33] = 74;
//            coder_codes_tab[34] = 36;
//            coder_codes_tab[35] = 25;
//            coder_codes_tab[36] = 8;
//            coder_codes_tab[37] = 6;
//            coder_codes_tab[38] = 62;
//            coder_codes_tab[39] = 2;
//            coder_codes_tab[40] = 12;
//            coder_codes_tab[41] = 21;
//            coder_codes_tab[42] = 40;
//            coder_codes_tab[43] = 49;
//            coder_codes_tab[44] = 35;
//            coder_codes_tab[45] = 34;
//            coder_codes_tab[46] = 19;
//            coder_codes_tab[47] = 3;
//            coder_codes_tab[48] = 17;
//            coder_codes_tab[49] = 22;
            
            coder_codes_tab_length[0] = 3;
            coder_codes_tab_length[1] = 7;
            coder_codes_tab_length[2] = 7;
            coder_codes_tab_length[3] = 7;
            coder_codes_tab_length[4] = 5;
            coder_codes_tab_length[5] = 7;
            coder_codes_tab_length[6] = 6;
            coder_codes_tab_length[7] = 6;
            coder_codes_tab_length[8] = 5;
            coder_codes_tab_length[9] = 7;
            coder_codes_tab_length[10] = 7;
            coder_codes_tab_length[11] = 7;
            coder_codes_tab_length[12] = 5;
            coder_codes_tab_length[13] = 7;
            coder_codes_tab_length[14] = 6;
            coder_codes_tab_length[15] = 6;
            coder_codes_tab_length[16] = 6;
            coder_codes_tab_length[17] = 5;
            coder_codes_tab_length[18] = 5;
            coder_codes_tab_length[19] = 6;
            coder_codes_tab_length[20] = 6;
            coder_codes_tab_length[21] = 6;
            coder_codes_tab_length[22] = 6;
            coder_codes_tab_length[23] = 7;
            coder_codes_tab_length[24] = 6;
            coder_codes_tab_length[25] = 6;
            coder_codes_tab_length[26] = 5;
//            coder_codes_tab_length[27] = 4;
//            coder_codes_tab_length[28] = 7;
//            coder_codes_tab_length[29] = 6;
//            coder_codes_tab_length[30] = 6;
//            coder_codes_tab_length[31] = 5;
//            coder_codes_tab_length[32] = 7;
//            coder_codes_tab_length[33] = 7;
//            coder_codes_tab_length[34] = 7;
//            coder_codes_tab_length[35] = 5;
//            coder_codes_tab_length[36] = 6;
//            coder_codes_tab_length[37] = 6;
//            coder_codes_tab_length[38] = 6;
//            coder_codes_tab_length[39] = 6;
//            coder_codes_tab_length[40] = 5;
//            coder_codes_tab_length[41] = 5;
//            coder_codes_tab_length[42] = 6;
//            coder_codes_tab_length[43] = 6;
//            coder_codes_tab_length[44] = 6;
//            coder_codes_tab_length[45] = 6;
//            coder_codes_tab_length[46] = 7;
//            coder_codes_tab_length[47] = 6;
//            coder_codes_tab_length[48] = 6;
//            coder_codes_tab_length[49] = 5;

            
        end

        always @(posedge clock or negedge reset) begin
            if(!reset) begin
                state = `CODEC_LOAD_DATA;
                input_data_count = 8'b0;
                previous_data_enable = 1'b0;
                //tree_data_enable = 1'b0;
                coder_data_enable = 1'b0;
                decoder_data_enable = 1'b0;
                ready_for_coded_data_prev = 1'b0;
                ready_for_decoded_data_prev = 1'b0;
                coded_out_data_ready = 1'b0;
                decoded_out_data_ready = 1'b0;
                decoded_out = 8'b0;
                coded_out = 1'b0;
            end
			else begin
            case(state)
                `CODEC_LOAD_DATA: begin
                    if(data_enable != previous_data_enable) begin
                        previous_data_enable = data_enable;
                        input_data_count = data_count;
                        coder_symbols_amount = symbols_amount;
                        decoder_symbols_amount = symbols_amount;
                        if(i < data_count) begin
                            data_input_tab[i] = data_in;
                            i = i + 1;
                            state = `CODEC_LOAD_DATA;
                        end
                        else begin
                            i = 0;
                            //tree_symbols_length = 50;
                            //state = `TREE_LOAD_DATA;
                            state = `CODER_LOAD_TREE_DATA;
                        end
                    end
                    else begin
                        state = `CODEC_LOAD_DATA;
                    end
                end
                
//                `TREE_LOAD_DATA: begin
//                    if(i < symbols_amount) begin
//                        tree_data_enable = 1;
//                        tree_symbols_length = 50;
//                        tree_symbol_in = symbols[i];
//                        tree_probability_in = probabilities[i];
//                        i = i + 1;
//                        state = `TREE_LOAD_DATA;
//                    end
//                    else begin
//                        i = 0;
//                        state = `TREE_CALC;
//                    end
//                end
                
//                `TREE_CALC: begin
//                    tree_data_ready_output = tree_data_ready;
//                    if(tree_data_ready_output) begin
//                        if(i < symbols_amount) begin
//                            coder_codes_tab[i] = tree_codes;
//                            coder_codes_tab_length[i] = tree_codes_length;
//                            i = i + 1;
//                            state = `TREE_CALC;
//                        end
//                        else begin
//                        //for(k=0; k < symbols_amount; k = k + 1) $display("symbol: ", symbols[k], " code: ", coder_codes_tab[k], " length: ", coder_codes_tab_length[k]);
//                            state = `CODER_LOAD_TREE_DATA;
//                            i = 0;
//                        end
//                    end
//                end
                
                `CODER_LOAD_TREE_DATA: begin
                    if(i < symbols_amount) begin
                        coder_data_enable = 1;
                        coder_code_in = coder_codes_tab[i];
                        coder_code_length_in = coder_codes_tab_length[i];
                        coder_symbols_in = symbols[i];
                        i = i + 1;
                        state = `CODER_LOAD_TREE_DATA;
                    end
                    else begin
                        i = 0;
                        coder_data_enable = 0;
                        state = `CODER_LOAD_INPUT_DATA;
                    end
                end
                
                `CODER_LOAD_INPUT_DATA: begin
                    if(i < input_data_count) begin
                        coder_data_enable = 1;
                        coder_data_input = data_input_tab[i];
                        i = i + 1;
                        state = `CODER_LOAD_INPUT_DATA;
                    end
                    else begin
                        i = 0;
                        state = `CODER_CALC;
                    end
                end

                `CODER_CALC: begin
                    coder_data_ready_output = coder_data_ready;
                    if(coder_data_ready_output) begin
                        decoder_bit_input[bit_counter] = coder_bit_output;
                        //$display("bit: ", decoder_bit_input[bit_counter]);
                        bit_counter = bit_counter + 1; 
                        state = `CODER_CALC;
                    end
                    else if(coder_data_ready_output == 0 && bit_counter > 0) begin
                        i = 0;
                        decoder_data_length_in = bit_counter;
                        state = `DECODER_LOAD_TREE_DATA;
                    end
                end  
                
                `DECODER_LOAD_TREE_DATA: begin
                     if(i < symbols_amount) begin
                         decoder_data_enable = 1;
                         decoder_code_in = coder_codes_tab[i];
                         decoder_code_length_in = coder_codes_tab_length[i];
                         decoder_symbols_in = symbols[i];
                         i = i + 1;
                         state = `DECODER_LOAD_TREE_DATA;
                     end
                     else begin
                         i = 0;
                         decoder_data_enable = 0;
                         state = `DECODER_LOAD_INPUT_DATA;
                     end  
                end
                
                `DECODER_LOAD_INPUT_DATA: begin
                    if(i < bit_counter) begin
                        decoder_data_enable = 1;
                        //decoder_data_length_in = bit_counter;
                        decoder_data_in = decoder_bit_input[i];
                        i = i + 1;
                        state = `DECODER_LOAD_INPUT_DATA;
                    end
                    else begin
                        i = 0;
                        state = `DECODER_CALC;
                    end
                end
                
                `DECODER_CALC: begin
                    decoder_data_ready_output = decoder_data_ready;
                    if(decoder_data_ready_output) begin
                        decoded_output[i] = decoder_decoded_number;
                        decoded_symbols_number = decoded_symbols_number+1;
                        $display("Decoded: ",decoded_output[i],"counter: ",decoded_symbols_number); 
                        i = i + 1;
                        state = `DECODER_CALC;
                    end
                    else if(decoder_data_ready_output == 0 && decoded_symbols_number > 0) begin
                        i = 0;
                        k = 0;
                        state = `WRITE_DATA_OUT;
                    end
                end
                
                `WRITE_DATA_OUT: begin
                    if(i < bit_counter) begin
                        coded_out_data_ready = 1;
                        if(ready_for_coded_data != ready_for_coded_data_prev) begin
                            ready_for_coded_data_prev = ready_for_coded_data;
                            coded_out = decoder_bit_input[i];
                            i = i+1;
                        end
                        state = `WRITE_DATA_OUT;
                    end
                    else begin
                        coded_out_data_ready = 0;
                        state = `CODEC_LOAD_DATA;
                    end
                    if(k < decoded_symbols_number) begin
                        decoded_out_data_ready = 1;
                        if(ready_for_decoded_data != ready_for_decoded_data_prev) begin
                            ready_for_decoded_data_prev = ready_for_decoded_data;
                            decoded_out = decoded_output[k];
                            k = k+1;
                        end
                        state = `WRITE_DATA_OUT;
                    end
                    else begin
                        decoded_out_data_ready = 0;
                    end
                end
                
                default: begin
                    // empty
                end
            endcase
			end
            

        end    
endmodule
