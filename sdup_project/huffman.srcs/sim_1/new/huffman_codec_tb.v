`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.05.2019 10:56:03
// Design Name: 
// Module Name: huffman_codec_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module huffman_codec_tb;

reg clock;
reg data_enable;
reg [7:0] data_in;
reg [7:0] data_count;
reg reset;
wire coded_out;
wire [7:0] decoded_out;
wire coded_data_ready;
wire decoded_data_ready;
reg ready_for_coded_data;
reg ready_for_decoded_data;

integer i = 32'h0;

reg [7:0] data_to_input [0:49] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122};

huffman_codec codec(
    .clock(clock),
    .reset(reset),
    .data_enable(data_enable),
    .data_in(data_in),
    .data_count(data_count),
    .coded_out(coded_out),
    .decoded_out(decoded_out),
    .coded_out_data_ready(coded_data_ready),
    .decoded_out_data_ready(decoded_data_ready),
    .ready_for_coded_data(ready_for_coded_data),
    .ready_for_decoded_data(ready_for_decoded_data)
    );
    
    initial begin
        forever #10 clock = ~clock;
    end
    
    initial begin
        clock = 0;
        data_in = 0;
        data_enable = 0;
        data_count = 27;//50;
        ready_for_coded_data = 0;
        ready_for_decoded_data = 0;
        #10
        reset = 1;
        #10
        reset = 0;
        #10
        reset = 1;
        #10
        
         //data_enable = 1;
         
         for(i =0; i < 27; i = i + 1) begin //for(i =0; i < 50; i = i + 1) begin
            data_enable = !data_enable;
            data_in = data_to_input[i];
            #20;
         end
         
         data_enable = !data_enable;
         
         while(coded_data_ready != 1 && coded_data_ready != 1) begin
         #500;
         end
         while(coded_data_ready || decoded_data_ready) begin
            if(coded_data_ready == 1)begin
                ready_for_coded_data = !ready_for_coded_data;
            end
            if(decoded_data_ready == 1)begin
                ready_for_decoded_data = !ready_for_decoded_data;
            end
            //$display("code: ", coded_out);
            #700;
         end
                
        #10000000; //delay 2100ps to finish testing
        $finish; // inform that the test was finished
    end

endmodule
